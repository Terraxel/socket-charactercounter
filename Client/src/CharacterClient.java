import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;

public class CharacterClient
{
    private Socket client;

    private ObjectInputStream in;
    private ObjectOutputStream out;

    public CharacterClient(String host, int port)
    {
        this.InitializeClient(host, port);
    }

    public void SendMessage(String msg)
    {
        try
        {
            this.out.writeObject(msg);
        }
        catch (IOException e)
        {
            System.err.println("Erreur lors de l'envoi du message");
        }
    }

    public HashMap<Character, Integer> GetAnswer()
    {
        try
        {
            HashMap<Character, Integer> answer = (HashMap<Character, Integer>)this.in.readObject();
            return answer;
        }
        catch (IOException e)
        {
            System.err.println("Erreur de lecture");
        }
        catch (ClassNotFoundException e)
        {
            System.err.println("Warzazat ?");
        }

        return null;
    }

    private void InitializeClient(String host, int port)
    {
        try
        {
            this.client = new Socket(host, port);

            this.out = new ObjectOutputStream(this.client.getOutputStream());
            this.in = new ObjectInputStream(this.client.getInputStream());
        }
        catch (IOException e)
        {
            System.err.println("Erreur lors de la connexion au serveur");
        }
    }
}
