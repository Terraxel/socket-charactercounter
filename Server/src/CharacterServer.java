import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class CharacterServer
{
    private ServerSocket server;
    private Socket connectedClient;

    private ObjectInputStream in;
    private ObjectOutputStream out;

    public CharacterServer(int port)
    {
        this.InitializeServer(port);
    }

    private void InitializeServer(int port)
    {
        try
        {
            this.server = new ServerSocket(port);
        }
        catch (IOException e)
        {
            System.err.println("Erreur au démarrage du serveur");
        }
    }

    public void WaitForConnection()
    {
        try
        {
            this.connectedClient = this.server.accept();
            System.out.println("Un client vient de se connecter");

            this.out = new ObjectOutputStream(this.connectedClient.getOutputStream());
            this.in = new ObjectInputStream(this.connectedClient.getInputStream());
        }
        catch (IOException e)
        {
            System.err.println("Erreur !");
        }
    }

    public void ReceiveMsg()
    {
        try
        {
            String received = (String)this.in.readObject();
            System.out.println("Reçu : " + received);
            HashMap<Character, Integer> processed = ProcessMsg(received);

            this.SendMessage(processed);
        }
        catch (IOException e)
        {
            System.err.println("Erreur de lecture");
        }
        catch (ClassNotFoundException e)
        {
            System.err.println("Warzazat ?");
        }
    }

    public void SendMessage(HashMap<Character, Integer> message)
    {
        try
        {
            this.out.writeObject(message);
        }
        catch (IOException e)
        {
            System.err.println("Erreur lors de l'envoi");
        }
    }

    private static HashMap<Character, Integer> ProcessMsg(String msg)
    {
        HashMap<Character, Integer> map = new HashMap<>();

        for(int i = 0; i < msg.length(); i++)
        {
            if(!map.containsKey(msg.charAt(i)))
                map.put(msg.charAt(i), 1);
            else
                map.replace(msg.charAt(i), map.get(msg.charAt(i)) + 1);
        }

        return map;
    }
}
